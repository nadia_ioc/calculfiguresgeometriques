package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author nadia
 */
public class Rectangle implements FiguraGeometrica{
    private final double longitud;    
    private final double amplada;

    
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del rectangle: ");
        this.longitud = scanner.nextDouble();
        System.out.println("Introduïu l'amplada del rectangle: ");
        this.amplada = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return longitud * amplada;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2 * (longitud + amplada);
    }
}
