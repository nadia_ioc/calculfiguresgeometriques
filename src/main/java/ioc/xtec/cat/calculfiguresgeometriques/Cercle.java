/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author nadia
 */
public class Cercle implements FiguraGeometrica{
    private final double radi;
    
    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu el radi del cercle: ");
        this.radi = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return Math.PI * radi * radi;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2 * Math.PI * radi;
    }
}
